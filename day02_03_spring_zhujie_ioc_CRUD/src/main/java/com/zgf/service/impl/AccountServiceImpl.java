package com.zgf.service.impl;

import com.zgf.dao.AccountDao;
import com.zgf.domain.Account;
import com.zgf.service.AccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service("accountService")
public class AccountServiceImpl implements AccountService {
    @Autowired
    private AccountDao accountDao;



    public void setAccountDao(AccountDao accountDao) {
        this.accountDao = accountDao;
    }


    @Override
    public List<Account> listAccount() {
        return accountDao.listAccount();
    }

    @Override
    public Account findInById(int id) {
        return accountDao.findInById(id);
    }

    @Override
    public void InsertAccount(Account account) {
             accountDao.InsertAccount(account);
    }

    @Override
    public void deleteAccount(int id) {
             accountDao.deleteAccount(id);
    }

    @Override
    public void updateAccount(Account account) {
             accountDao.updateAccount(account);
    }


}
