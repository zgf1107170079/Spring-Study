package com.zgf.jdbcTemplate;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.jdbc.core.JdbcTemplate;

public class JdbcTemplateDemo2 {
    public static void main(String[] args) {
        ApplicationContext ac =new ClassPathXmlApplicationContext("applicationContext.xml");

        JdbcTemplate jt = ac.getBean("jdbcTemplate",JdbcTemplate.class);

        jt.execute("INSERT INTO Account (name,money) VALUES ('郑国锋',10000)");



    }
}
