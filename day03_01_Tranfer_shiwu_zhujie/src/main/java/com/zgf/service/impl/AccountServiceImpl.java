package com.zgf.service.impl;

import com.zgf.dao.IAccountDao;
import com.zgf.domain.Account;
import com.zgf.service.IAccountService;
import com.zgf.utils.TranscationManager;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * 账户的业务层实现类
 */
@Component("accountService")
public class AccountServiceImpl implements IAccountService{
    @Resource
    private IAccountDao accountDao;

    @Override
    public List<Account> findAllAccount() {
            return  accountDao.findAllAccount();
    }

    @Override
    public Account findAccountById(Integer accountId) {
        return  accountDao.findAccountById(accountId);

    }

    @Override
    public void saveAccount(Account account) {

            accountDao.saveAccount(account);


    }

    @Override
    public void updateAccount(Account account) {

            accountDao.updateAccount(account);



    }

    @Override
    public void deleteAccount(Integer acccountId) {

            accountDao.deleteAccount(acccountId);


    }



    @Override
    public void tranfer(String sourceName, String targetName, Float money) {

            Account source = accountDao.findAccountByName(sourceName);
            //2.2.根据名称查询转入账户
            Account target = accountDao.findAccountByName(targetName);
            //2.3.转出账户减钱
            source.setMoney(source.getMoney()-money);
            //2.4.转入账户增钱
            target.setMoney(target.getMoney()+money);
            //2.5.更新转出账户
            accountDao.updateAccount(source);

             int i=1/0;

            //6.更新转入账户
            accountDao.updateAccount(target);




    }


}
