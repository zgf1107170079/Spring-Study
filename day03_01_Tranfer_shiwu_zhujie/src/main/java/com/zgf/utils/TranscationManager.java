package com.zgf.utils;

import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.sql.SQLException;
@Component("txManager")
public class TranscationManager {
@Resource
  private ConnectionUtils connectionUtils;



    public   void beginTranscation(){
        try {
            connectionUtils.getThreadConnecation().setAutoCommit(false);
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    public  void commit(){
        try {
            connectionUtils.getThreadConnecation().commit();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }



    public  void rollback(){
        try {
            connectionUtils.getThreadConnecation().rollback();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }



    public  void release(){
        try {
            connectionUtils.getThreadConnecation().close();
            connectionUtils.removeConection();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }




}
