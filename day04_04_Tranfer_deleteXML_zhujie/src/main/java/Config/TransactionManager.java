package Config;


import org.springframework.context.annotation.Bean;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.transaction.PlatformTransactionManager;

import javax.sql.DataSource;

/**
 * 事务相关类*/
public class TransactionManager {

    @Bean(name = "transactionManager")
    public PlatformTransactionManager creatTranstionManager(DataSource dataSource){
        return new DataSourceTransactionManager(dataSource);

    }
}
