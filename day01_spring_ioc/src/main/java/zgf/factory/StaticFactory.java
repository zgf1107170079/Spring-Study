package zgf.factory;

import zgf.service.IAccountService;
import zgf.service.impl.AccountServiceImpl;

public class StaticFactory {

    public static IAccountService createAccountService(){
        return new AccountServiceImpl();
    }
}
