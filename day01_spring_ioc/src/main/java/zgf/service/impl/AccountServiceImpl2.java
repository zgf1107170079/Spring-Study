package zgf.service.impl;

import zgf.service.IAccountService;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class AccountServiceImpl2 implements IAccountService {

    private String[] Str;
    private List<String> list;
    private Set<String> set;
    private Map<String,String> map;


    public void setStr(String[] str) {
        Str = str;
    }

    public void setList(List<String> list) {
        this.list = list;
    }

    public void setSet(Set<String> set) {
        this.set = set;
    }

    public void setMap(Map<String, String> map) {
        this.map = map;
    }

    @Override
    public void saveAccount() {
        System.out.println(Arrays.toString(Str));
        System.out.println(list);
        System.out.println(set);
        System.out.println(map);
    }
}
