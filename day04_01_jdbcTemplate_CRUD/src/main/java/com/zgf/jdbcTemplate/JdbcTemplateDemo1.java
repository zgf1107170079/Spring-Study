package com.zgf.jdbcTemplate;

import com.zgf.domain.Account;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.jdbc.datasource.DriverManagerDataSource;

public class JdbcTemplateDemo1 {

    public static void main(String[] args) {
        DriverManagerDataSource ds = new DriverManagerDataSource();
        ds.setDriverClassName("com.mysql.jdbc.Driver");
        ds.setUrl("jdbc:mysql://localhost:3306/how2java");
        ds.setUsername("root");
        ds.setPassword("123456");

        org.springframework.jdbc.core.JdbcTemplate jt =new org.springframework.jdbc.core.JdbcTemplate();

        jt.setDataSource(ds);
        jt.execute("INSERT INTO account  (NAME,money)  VALUES ('哈哈哈',5000)");

     }

}
