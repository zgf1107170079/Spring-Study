package com.zgf.utils;

import java.sql.SQLException;

public class TranscationManager {

  private ConnectionUtils connectionUtils;



    public void setConnectionUtils(ConnectionUtils connectionUtils) {
        this.connectionUtils = connectionUtils;
    }



    public   void beginTranscation(){
        try {
            connectionUtils.getThreadConnecation().setAutoCommit(false);
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }



    public  void commit(){
        try {
            connectionUtils.getThreadConnecation().commit();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }



    public  void rollback(){
        try {
            connectionUtils.getThreadConnecation().rollback();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }



    public  void release(){
        try {
            connectionUtils.getThreadConnecation().close();
            connectionUtils.removeConection();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }




}
