package com.zgf.utils;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.SQLException;

public class ConnectionUtils {

    private ThreadLocal<Connection> tl = new ThreadLocal<Connection>();

    public DataSource dataSource;

    public void setDataSource(DataSource dataSource) {
        this.dataSource = dataSource;
    }


    /**
     * 获取当前线程上的连接
     * */
    public Connection getThreadConnecation(){

        try {
            Connection conn = tl.get();
            if (conn == null){
                conn = dataSource.getConnection();
                tl.set(conn);
            }
            return conn;
        }catch (SQLException e) {
          throw new RuntimeException(e);
        }
    }


    /**
     * 把链接和线程解绑
     * */
    public void removeConection(){
        tl.remove();
    }
}
