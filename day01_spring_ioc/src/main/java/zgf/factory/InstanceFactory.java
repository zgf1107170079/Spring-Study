package zgf.factory;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import zgf.service.IAccountService;
import zgf.service.impl.AccountServiceImpl;

public class InstanceFactory {

    public IAccountService createAccountService(){
        return new AccountServiceImpl();
    }

}
