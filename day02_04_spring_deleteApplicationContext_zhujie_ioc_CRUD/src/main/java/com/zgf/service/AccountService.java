package com.zgf.service;

import com.zgf.domain.Account;

import java.util.List;

public interface AccountService {

/**查询所有
* */

List<Account> listAccount();


/**
 * 单个查询
 * */
Account findInById(int id);






/**
 *
 * 增加
 * */


void InsertAccount(Account account);
/**
 * 删除
 * */

void deleteAccount(int id);




/**
 * 修改
 * */

void updateAccount(Account account);





}
