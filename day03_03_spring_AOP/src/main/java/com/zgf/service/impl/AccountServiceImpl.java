package com.zgf.service.impl;

import com.zgf.service.AccountService;
import org.aspectj.lang.annotation.Pointcut;



public class AccountServiceImpl implements AccountService {


    @Override
    public void saveAccount() {
        System.out.println("账户已保存。。。。。。");
    }

    @Override
    public void deleteAccount(int i) {
        System.out.println("账户已删除。。。。。。"+i);
    }

    @Override
    public int updateAccount() {
        System.out.println("账户已修改。。。。。。");
        return 0;
    }

}
