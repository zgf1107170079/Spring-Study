package com.zgf.dao;

import com.zgf.domain.Account;

import java.util.List;

public interface AccountDao {


    /**查询所有
     * */

    List<Account> listAccount();

    /**
     * 单个查询
     * */
    Account findInById(int id);


    /**
     *
     * 增加
     * */


    void InsertAccount(Account account);
    /**
     * 删除
     * */

    void deleteAccount(int id);




    /**
     * 修改
     * */

    void updateAccount(Account account);



}
