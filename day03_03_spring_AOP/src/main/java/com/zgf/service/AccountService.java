package com.zgf.service;

public interface AccountService {

    void saveAccount();

    void deleteAccount(int i);

    int updateAccount();
}
