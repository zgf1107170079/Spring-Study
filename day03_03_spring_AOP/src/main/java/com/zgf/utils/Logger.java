package com.zgf.utils;


import org.aspectj.lang.ProceedingJoinPoint;
import org.springframework.mock.web.MockMultipartFile;

public class Logger {

    /**
     * 前置通知
     */
    public  void beforePrintLog(){
        System.out.println("前置通知Logger类中的beforePrintLog方法开始记录日志了。。。");
    }

    /**
     * 后置通知
     */
    public  void afterReturningPrintLog(){
        System.out.println("后置通知Logger类中的afterReturningPrintLog方法开始记录日志了。。。");
    }
    /**
     * 异常通知
     */
    public  void afterThrowingPrintLog(){
        System.out.println("异常通知Logger类中的afterThrowingPrintLog方法开始记录日志了。。。");
    }

    /**
     * 最终通知
     */
    public  void afterPrintLog(){
        System.out.println("最终通知Logger类中的afterPrintLog方法开始记录日志了。。。");
    }



    public Object aroundPrintLog(ProceedingJoinPoint pjp){
        Object rtValue=null;
        try {
            Object[] args = pjp.getArgs();//得到参数的方法

            System.out.println("前置通知");

            rtValue =pjp.proceed();

            System.out.println("后置通知");
        } catch (Throwable throwable) {
            new RuntimeException(throwable);
            System.out.println("异常通知");
        }finally {
            System.out.println("最终通知");
        }
        return rtValue;
    }
}
