package com.zgf.test;

import com.zgf.domain.Account;
import com.zgf.service.AccountService;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.util.List;

/**
 *
 * 使用junit测试
 * */
public class AccountServiceTest {

    @Test
    public void testList() throws Exception {
        ApplicationContext ac =new ClassPathXmlApplicationContext("applicationContext.xml");
        AccountService as = ac.getBean("accountService", AccountService.class);
        List<Account> accounts = as.listAccount();
        for (Account account : accounts) {
        System.out.println(account);
    }
}


    @Test
    public void testSelect() throws Exception {
        ApplicationContext ac =new ClassPathXmlApplicationContext("applicationContext.xml");
        AccountService as = ac.getBean("accountService", AccountService.class);
        Account accounts = as.findInById(1);
        System.out.println(accounts);
    }



    @Test
    public void testInsert() throws Exception {
        Account account = new Account();
        account.setName("奥特曼");
        account.setMoney("500");
        ApplicationContext ac =new ClassPathXmlApplicationContext("applicationContext.xml");
        AccountService as = ac.getBean("accountService", AccountService.class);
        as.InsertAccount(account);
    }


    @Test
    public void testUpdate() throws Exception {
        ApplicationContext ac =new ClassPathXmlApplicationContext("applicationContext.xml");
        AccountService as = ac.getBean("accountService", AccountService.class);
        Account account = as.findInById(4);
        account.setMoney("1000");
        as.updateAccount(account);
    }
    @Test
    public void testDelete() throws Exception {
        ApplicationContext ac =new ClassPathXmlApplicationContext("applicationContext.xml");
        AccountService as = ac.getBean("accountService", AccountService.class);
        as.deleteAccount(4);
    }

}
