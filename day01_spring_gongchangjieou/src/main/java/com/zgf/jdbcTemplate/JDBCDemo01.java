package com.zgf.jdbcTemplate;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;


/**
 *
 * 程序的耦合
 *      耦合：程序见间的关系
 *
 *            类之间的依赖
 *            方法间的依赖
 *
 *
 * */

public class JDBCDemo01 {
    public static void main(String[] args) throws Exception {
        //注册驱动
        DriverManager.registerDriver(new com.mysql.jdbc.Driver());
        //获取链接
        Connection conn = DriverManager.getConnection("jdbcTemplate:mysql://127.0.0.1:3306/how2java?characterEncoding=UTF-8",
                "root", "123456");
        //获取数据库操作预处理对象
        PreparedStatement pstm = conn.prepareStatement("SELECT * FROM order_");
        //执行sql语句，得到结果集
        ResultSet rs =pstm.executeQuery();

        //遍历数据
        while(rs.next()){
            System.out.println(rs.getInt("id")+"\t"+rs.getString("code"));
        }

    }
}
