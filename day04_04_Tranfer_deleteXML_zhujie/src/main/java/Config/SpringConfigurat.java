package Config;


import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.context.annotation.PropertySource;
import org.springframework.transaction.annotation.EnableTransactionManagement;


/**
 * Spring配置类 相当于ApplicationContext
* */
@Configuration
@ComponentScan("com.zgf")
@Import({JdbcConfig.class,TransactionManager.class})
@PropertySource("jdbc.properties")
@EnableTransactionManagement
public class SpringConfigurat {

}
