package com.zgf.dao;

import com.zgf.domain.Account;
import sun.reflect.generics.scope.Scope;

import java.util.List;

public interface AccountDao {

    public Account findById(int id);

    public Account findByName(String name);

    public  void update(int id);

    public void listAccount();

}
