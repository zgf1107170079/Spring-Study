package com.zgf.proxy;

public interface Product {

    /**
     * 销售*/

     void saleProduct(float money);



     /**
      * 售后*/
     void afterSale(float money);
}
