package com.zgf.test;

import Config.SpringConfigurat;
import com.zgf.service.IAccountService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 * 使用Junit单元测试：测试我们的配置
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = SpringConfigurat.class)
public class AccountServiceTest {

    @Autowired
    private  IAccountService as;



    @Test
    public  void testTransfer(){
        as.transfer("郑国锋","曹瀛月",100f);
    }

}
