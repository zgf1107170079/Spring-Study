package com.zgf.proxy;

public class ProductImpl implements Product {


    @Override
    public void saleProduct(float money) {
        System.out.println("出售的价格："+money);
    }

    @Override
    public void afterSale(float money) {
        System.out.println("售后服务价钱："+money);
    }
}
