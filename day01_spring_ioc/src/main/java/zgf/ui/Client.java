package zgf.ui;


import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import zgf.service.IAccountService;

/**
 * 模拟一个表现层，用于调用业务层
 */
public class Client {


    /**
     * 获取springIOC容器，根据id获取对象
     */
    public static void main(String[] args) {

        ApplicationContext ac = new ClassPathXmlApplicationContext("ApplicationContext.xml");
        /*   构造函数注入*/
        IAccountService as = (IAccountService) ac.getBean("accountService");
        as.saveAccount();

        /*set方法注入*/
/*        IAccountService as = (IAccountService) ac.getBean("accountService1");
        as.saveAccount(); */

     /*   IAccountService as = (IAccountService) ac.getBean("accountService2");
        as.saveAccount();*/


    }
}
