package com.zgf.factory;

import com.zgf.utils.TranscationManager;
import org.aspectj.lang.ProceedingJoinPoint;

import javax.annotation.Resource;

public class AOP {

    @Resource
    private TranscationManager txManager;

    public Object arround(ProceedingJoinPoint pjp){
        Object rtValue = null;
        try {
            Object[] arg =pjp.getArgs();

            System.out.println("前置通知！");
            txManager.beginTranscation();
            rtValue=pjp.proceed();
            System.out.println("后置通知！");
            txManager.commit();
        } catch (Throwable throwable) {
            System.out.println("异常通知！");
            txManager.rollback();
        }finally {
            System.out.println("最终通知！");
            txManager.release();
        }
         return  rtValue;
    }
}
