package zgf.service.impl;


import zgf.dao.IAccountDao;
import zgf.dao.impl.IAccountDaoImpl;
import zgf.service.IAccountService;

import java.util.Date;

/**
 * 账户的业务层实现类
 */
public class AccountServiceImpl implements IAccountService {

    private IAccountDao accountDao = new IAccountDaoImpl();

    private String name;
    private Integer age;
    private Date birthday;

    public AccountServiceImpl(String name, Integer age, Date birthday) {
        this.name = name;
        this.age = age;
        this.birthday = birthday;
    }


    public AccountServiceImpl(){
        System.out.println("对象已经创建!");
    }


    public void  saveAccount(){
        System.out.println("AccountServiceImpl{" +
                "name='" + name + '\'' +
                ", age=" + age +
                ", birthday=" + birthday +
                '}');
    }



}
