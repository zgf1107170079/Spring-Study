package com.zgf.ui;

import com.zgf.config.TestConfiguration;
import com.zgf.dao.IAccountDao;
import com.zgf.service.IAccountService;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * 模拟一个表现层，用于调用业务层
 */
public class Client {

    /**
     *
     *
     *
     *
     *
     * @param args
     */

    public static void main(String[] args) {
        //1.获取核心容器对象
/*        ApplicationContext ac = new ClassPathXmlApplicationContext("applicationContext.xml");

        IAccountService as  = (IAccountService)ac.getBean("accountServiceImpl");

        as.saveAccount();*/

        AnnotationConfigApplicationContext ctx = new AnnotationConfigApplicationContext();
       // ApplicationContext ctx = new ClassPathXmlApplicationContext("applicationContext.xml");
        ctx.scan("com.zgf");
        ctx.refresh();
        TestConfiguration accountService = ctx.getBean(TestConfiguration.class);
        IAccountService iAccountService = accountService.accountService();
        IAccountService iAccountService1 = accountService.accountService1();
        if (iAccountService.hashCode()==iAccountService1.hashCode()){
            System.out.println("true");
        }
        iAccountService1.saveAccount();
        iAccountService.saveAccount();


    }
}
