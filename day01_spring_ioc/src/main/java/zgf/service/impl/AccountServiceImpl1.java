package zgf.service.impl;

import zgf.service.IAccountService;

import java.util.Date;

public class AccountServiceImpl1 implements IAccountService {

    private String name;
    private Integer age;
    private Date birthday;




    public void setName(String name) {
        this.name = name;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public void setBirthday(Date birthday) {
        this.birthday = birthday;
    }


    public AccountServiceImpl1() {

    }

    @Override
    public void  saveAccount(){
        System.out.println("AccountServiceImpl{\n" +
                "name='" + name + '\'' +
                ", age=" + age +
                ", birthday=" + birthday +
                '}');
    }

}
